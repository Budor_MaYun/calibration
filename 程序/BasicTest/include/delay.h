#ifndef __delay
#define __delay

void delay_us(unsigned int _us);
void delay_ms(unsigned int _us);

#endif	 