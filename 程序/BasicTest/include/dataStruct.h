#ifndef __DATASTRUCT_H
#define __DATASTRUCT_H

void saveByteData(u16 usAddr, u8 ucData);
u8 getData(u16 usAddress);

/*
总共可用 32字节
分配：
波特率（1）频率（1）重传次数（1）                     3
发送ID（5）接收ID0（5）接收ID1（5）接收ID2~ID5（4）   19
传输速度（1）
是否返回（1）
是否ACK（1）
中断电平（1）
工作模式（1）
发送功率（1）
*/


#define STRUCT_BEGIN_ADDR              (FLASH_DATA_START_PHYSICAL_ADDRESS + 96)
#define STRUCT_BAUD_ADDR               (STRUCT_BEGIN_ADDR)
#define STRUCT_FREQ_ADDR               (STRUCT_BAUD_ADDR+1)
#define STRUCT_RETRY_ADDR              (STRUCT_FREQ_ADDR+1)
#define STRUCT_BACK_ADDR               (STRUCT_RETRY_ADDR+1)
#define STRUCT_ACK_ADDR                (STRUCT_BACK_ADDR+1)
#define STRUCT_POWER_ADDR              (STRUCT_ACK_ADDR+1)
#define STRUCT_SPEED_ADDR              (STRUCT_POWER_ADDR+1)
#define STRUCT_FLAG_ADDR               (STRUCT_SPEED_ADDR+1)

#define STRUCT_TID0_ADDR               (STRUCT_BEGIN_ADDR+10)
#define STRUCT_TID1_ADDR               (STRUCT_TID0_ADDR+1)
#define STRUCT_TID2_ADDR               (STRUCT_TID1_ADDR+1)
#define STRUCT_TID3_ADDR               (STRUCT_TID2_ADDR+1)
#define STRUCT_TID4_ADDR               (STRUCT_TID3_ADDR+1)
#define STRUCT_RID0_ADDR               (STRUCT_TID4_ADDR+1)
#define STRUCT_RID1_ADDR               (STRUCT_RID0_ADDR+1)
#define STRUCT_RID2_ADDR               (STRUCT_RID1_ADDR+1)
#define STRUCT_RID3_ADDR               (STRUCT_RID2_ADDR+1)
#define STRUCT_RID4_ADDR               (STRUCT_RID3_ADDR+1)


#define getBaud()                      getData(STRUCT_BAUD_ADDR)
#define getFreq()                      getData(STRUCT_FREQ_ADDR)
#define getRetry()                     getData(STRUCT_RETRY_ADDR)
#define getBack()                      getData(STRUCT_BACK_ADDR)
#define getAck()                       getData(STRUCT_ACK_ADDR)
#define getPower()                     getData(STRUCT_POWER_ADDR)
#define getSpeed()                     getData(STRUCT_SPEED_ADDR)
#define getFlag()                      getData(STRUCT_FLAG_ADDR)

#define getTID0()                      getData(STRUCT_TID0_ADDR)
#define getTID1()                      getData(STRUCT_TID1_ADDR)
#define getTID2()                      getData(STRUCT_TID2_ADDR)
#define getTID3()                      getData(STRUCT_TID3_ADDR)
#define getTID4()                      getData(STRUCT_TID4_ADDR)
#define getRID0()                      getData(STRUCT_RID0_ADDR)
#define getRID1()                      getData(STRUCT_RID1_ADDR)
#define getRID2()                      getData(STRUCT_RID2_ADDR)
#define getRID3()                      getData(STRUCT_RID3_ADDR)
#define getRID4()                      getData(STRUCT_RID4_ADDR)

#define saveBaud(d)                    saveByteData(STRUCT_BAUD_ADDR, d)
#define saveFreq(d)                    saveByteData(STRUCT_FREQ_ADDR, d)
#define saveRetry(d)                   saveByteData(STRUCT_RETRY_ADDR, d)
#define saveBack(d)                    saveByteData(STRUCT_BACK_ADDR, d)
#define saveAck(d)                     saveByteData(STRUCT_ACK_ADDR, d)
#define savePower(d)                   saveByteData(STRUCT_POWER_ADDR, d)
#define saveSpeed(d)                   saveByteData(STRUCT_SPEED_ADDR, d)
#define saveFlag(d)                    saveByteData(STRUCT_FLAG_ADDR, d)

#define saveTID0(d)                    saveByteData(STRUCT_TID0_ADDR, d)
#define saveTID1(d)                    saveByteData(STRUCT_TID1_ADDR, d)
#define saveTID2(d)                    saveByteData(STRUCT_TID2_ADDR, d)
#define saveTID3(d)                    saveByteData(STRUCT_TID3_ADDR, d)
#define saveTID4(d)                    saveByteData(STRUCT_TID4_ADDR, d)
#define saveRID0(d)                    saveByteData(STRUCT_RID0_ADDR, d)
#define saveRID1(d)                    saveByteData(STRUCT_RID1_ADDR, d)
#define saveRID2(d)                    saveByteData(STRUCT_RID2_ADDR, d)
#define saveRID3(d)                    saveByteData(STRUCT_RID3_ADDR, d)
#define saveRID4(d)                    saveByteData(STRUCT_RID4_ADDR, d)


#define STRUCT_UPDATE                  (FLASH_DATA_START_PHYSICAL_ADDRESS + 0x7F)
#define saveUpdate()                   saveByteData(STRUCT_UPDATE, 0x55)

#endif
