#ifndef __CONFIG_H
#define __CONFIG_H
#include "stm8s.h"
#include "uart.h"
#include "delay.h"
#include "gpio.h"

#define TIME_SPAN						42000
#define MCU_FREQ                        24000000
#define resetIWDG()                     IWDG->KR = IWDG_KEY_REFRESH
#define LED1_ON()                       GPIOD->ODR &= ~(1<<3);
#define LED1_OFF()                      GPIOD->ODR |= 1<<3;

#ifndef NULL                    
#define NULL                           (void*)0
#endif

#define SET_CLK_HSI()                   CLK->SWCR|=CLK_SWCR_SWEN;CLK->SWR=0xE1;while((CLK->SWCR&0x01)!=0);\
                                        CLK->PCKENR1=(1<<3)
#define SET_CLK_LSI()                   CLK->SWCR|=CLK_SWCR_SWEN;CLK->SWR=0xD2;while((CLK->SWCR&0x01)!=0);\
                                        CLK->PCKENR1=0

typedef struct sensor{	
	u8 model;
	u16 id;
	u16 tempt;
	u16 frequency1;
	u16 frequency2;
	u16 frequency3;
}SENSOR;

extern u32 swtch; 
void Init_Iwdg(void);
#endif


