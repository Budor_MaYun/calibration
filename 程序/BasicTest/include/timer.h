#ifndef __TIMER_H
#define __TIMER_H

void Timer4Init(void);
void resetTime2Cntr(void);
void Time2ForResetInit(void);
void Timer2Uninit(void);
void Timer1_Init(void);
void Timer1_UnInit(void);
void Timer2Init(void);
void Timer4_UnInit(void);

#endif
