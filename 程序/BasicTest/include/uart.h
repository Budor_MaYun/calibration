#ifndef __UART_H
#define __UART_H

#include "config.h"
//#include <stdio.h>

#define BAUD_RATE                  9600

#define UARTPORT                   UART1
#define UART_FLAG(FLAG)            UART1_##FLAG

void UARTInit(void);
void uartSetBaudRate(u8 ulBaudRate);
void uartSendData(u8 ucData);
void uartSendNData(u8 *pucData, u8 ucLength);
void uartSendString(u8 *ucString);
u8 uartReceiveData(u8 *pucData);
u8 uartReceiveNData(u8 *pucData);

#endif

