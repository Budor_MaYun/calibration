#include "config.h"
#include "timer.h"

void Timer1_Init(void){				
	CLK->PCKENR1 |= (1<<7);
    TIM1->ARRL = (39>>0) & 0xFF;
    TIM1->ARRH = (39>>8) & 0xFF;
    TIM1->PSCRH = (0>>8) & 0xFF;; 	// 预分频寄存器
	TIM1->PSCRL = (0>>0) & 0xFF;; 	// 预分频寄存器
    TIM1->IER = 0x01;      //开中断
    TIM1->CR1 = (1<<0);
}

void Timer1_UnInit(void){				
	TIM1->CNTRH = 0;
  	TIM1->CNTRL = 0;
	TIM1->IER = 0;
	TIM1->IER = 0x00;              
	CLK->PCKENR1 &= ~(1<<7);
}

void Timer4Init(void){					// 定时器4中断  低优先级  系统时钟节拍定时器用
	CLK->PCKENR1 |= (1<<4);
    TIM4->ARR = 239;
    TIM4->PSCR = TIM4_PRESCALER_1; 		// 预分频寄存器
    TIM4->IER = 0x01;					//开中断
    TIM4->CR1 = (u8)((1<<7)|(1<<0));
}

void Timer4_UnInit(void){				// 定时器4中断  低优先级  系统时钟节拍定时器用
    TIM4->CNTR = 0;
	TIM4->IER = 0;
	TIM4->IER = 0x00;              
	CLK->PCKENR1 &= ~(1<<4);
}

void Timer2Init(void){
    CLK->PCKENR1 |= (1<<5);
    TIM2->ARRL = (65535 >> 0) & 0xFF;
    TIM2->ARRH = (65535 >> 8) & 0xFF;
    TIM2->PSCR = TIM2_PRESCALER_128; 		// 预分频寄存器
    TIM2->IER = 0;							//关中断
    TIM2->CR1 = (u8)((1<<7)|(1<<0));
}

void Timer2Uninit(void){ 
    TIM2->CNTRH = 0;
    TIM2->CNTRH = 0;
    CLK->PCKENR1 &= ~(1<<5);
}
void resetTime2Cntr(void){  
    TIM2->CNTRH = 0;
	TIM2->CNTRL = 0;
}

void Time2ForResetInit(void){ 			// 定时时间到自动停止   
    CLK->PCKENR1 |= (1<<5);
    resetTime2Cntr();
    TIM2->PSCR = TIM2_PRESCALER_16384; // 预分频寄存器
    TIM2->IER = 0x01;
    TIM2->CR1 = (u8)((1<<3)|(1<<0));
    
}
