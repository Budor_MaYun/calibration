#include "config.h"
#include "dataStruct.h"
#include "delay.h"

void saveByteData(u16 usAddr, u8 ucData){
    FLASH->DUKR = 0xAE;
    FLASH->DUKR = 0x56;
    delay_us(10); // 等待解锁成功
    *(__near uint8_t*) (uint16_t)usAddr = ucData;
    while(((FLASH->IAPSR) & (1<<2)) != 0); // 等待编程结束
    FLASH->IAPSR &= 0xF7;
}

u8 getData(u16 usAddress){
    return ((*(__near uint8_t *) (uint16_t)usAddress));
}
