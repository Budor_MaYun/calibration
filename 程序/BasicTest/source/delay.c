#include "delay.h"

void delay_us(unsigned int _us){
  	volatile unsigned int a = 0;
	while(_us--){
		for(a = 0;a < 1;a++)
				 ;
	}
}

void delay_ms(unsigned int _ms){  
      while (_ms--){
          delay_us(1000);
      }
}
