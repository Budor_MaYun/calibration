#include "config.h"
#include "gpio.h"

void GPIOInit(void){
      GPIOA -> DDR = 0;
      GPIOA -> CR1 = 0;
      GPIOA -> CR2 = 0;

      GPIOB -> DDR = (1<<4);
      GPIOB -> CR1 = (1<<4);
      GPIOB -> CR2 = (1<<4);
      GPIOB-> ODR &= ~(1<<4);

      GPIOC -> DDR = (1<<3)|(1<<4);
      GPIOC -> CR1 = 0xff;                //频率采集io
      GPIOC -> CR2 = (1<<3)|(1<<4);
      GPIOC -> ODR &= ~((1<<3)|(1<<4));

      ITC->ISPR2 |= (1<<2)|(1<<3); 		// PORTC优先级3
      EXTI->CR1 |= 0x20;                  //下降沿触发 	
      ITC->ISPR5 = 0XC3; 					// 串口接收高优先级 2  	串口发送优先级   2     高优先级

      GPIOD -> DDR = (1<<2)|(1<<3)|(1<<5);
      GPIOD -> CR1 = (1<<2)|(1<<6); 	// RXD上拉输入
      GPIOD -> CR2 = (1<<2)|(1<<3)|(1<<5);							// TXD高速输出
      GPIOD -> ODR |= (1<<2)|(1<<3)|(1<<5);  
      }



