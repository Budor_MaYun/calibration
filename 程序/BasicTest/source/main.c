#include "config.h"
#include "ds18b20.h"
#include "sensor.h"

double tempterp = 0;
SENSOR sensor = {0};
SENSOR *Psensor = &sensor;
u8 receivArr[4];
u8 type,revFlg,bdFlg = 1,tepFlg;

void SystemInit(void);

int main(){
    SystemInit();             
    GPIOInit();
    Timer2Init();
    disableInterrupts();
    UARTInit();
    led_shile();
    enableInterrupts();
		
    while( 1 ) {
		if(bdFlg){                 						//默认为标定模式
			  get_sensorInf(Psensor);
			  sendSensorData(Psensor);	
		} else {                                       //改型号模式
			  if(revFlg){
				  revFlg = 0;
				  setType(type);
				  sendTempData(Psensor,8);
			  }
			  if(tepFlg){
				  tepFlg = 0;
				  getTempType(&(Psensor->tempt),&(Psensor->model));
				  sendTempData(Psensor,9);
			  }
		}
    }
}

void SystemInit(void){
	//CLK->ICKR = 0x19; 								// 打开128K RC
	CLK->CKDIVR |= CLK_PRESCALER_CPUDIV1;               // CPU 时钟分频 1，CPU时钟 = 外部时钟(即是外部晶振频率)

    CLK->ECKR |= CLK_ECKR_HSEEN;                        // 允许外部高速振荡器工作
    while(!(CLK->ECKR & CLK_ECKR_HSERDY));              // 等待外部高速振荡器准备好

    CLK->SWCR |= CLK_SWCR_SWEN;                         // 使能切换
    CLK->SWR = CLK_SOURCE_HSE;                          // 选择芯片外部的高速振荡器为主时钟
    while(!(CLK->SWCR&CLK_SWCR_SWIF));                  // 等待切换成功
    CLK->SWCR &= ~(CLK_SWCR_SWEN|CLK_SWCR_SWIF);        // 清除切换标志
}
