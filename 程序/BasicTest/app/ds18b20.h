#ifndef DS18B20_H
#define DS18B20_H

#include "config.h"
#include "stm8s_gpio.h"
#include "delay.h"
#include "timer.h"

#define DQ_H()	GPIOB->ODR |= 1 << 5
#define DQ_L()	GPIOB->ODR &= ~(1 << 5)

extern void get_sensorInf(SENSOR *sensor);
extern void getTempType(u16 *temp,u8 *type);
extern void setType(u8 type);

#endif