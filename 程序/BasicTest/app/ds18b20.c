#include "ds18b20.h"

static void set_output(void){
    GPIOB -> DDR |= 1 << 5;          //���
  	GPIOB -> CR1 |= 1 << 5;       	 //�������
  	GPIOB -> CR2 |= 1 << 5;          //10M����
}

static void set_input(void){
    GPIOB -> DDR &= ~(1 << 5);       //���� 
  	GPIOB -> CR1 |= 1 << 5;      	 //������������
  	GPIOB -> CR2 &= ~(1 << 5);       //�ر��ж�
}

static void ds18b20_init(void){
      set_output();
	  DQ_L();
	  delay_us(900);
	  DQ_H();
	  delay_us(500);
}

static void ds18b20_writeBit(u8 data){
	u8 i = 0;
	set_output();
	for(i = 0;i<8;i++){
		DQ_L();
		delay_us(10);
		if((data & 0x01) == 1){
			DQ_H();
		}else{
			DQ_L();
		}
		delay_us(80);
		DQ_H();
		data >>= 1;
	}
}

static u8 ds18b20_readBit(void){
	u8 i = 0,value = 0;
	
	for(i = 0;i < 8;i++){
		value >>= 1;
		set_output();
		DQ_L();
		delay_us(5);
		DQ_H();
		delay_us(10);
		set_input();
		if(GPIOB->IDR & (1 << 5)){
			value |= 0x80;
		}
		delay_us(45);
	}
	return value;
}

/////////���ô������ͺ�///////////////
void setType(u8 type){
	ds18b20_init();
	ds18b20_writeBit(0xCC);         	//����ROMָ��
	ds18b20_writeBit(0x4E);         	//д���ݴ���ָ��
	ds18b20_writeBit(type);         	//����ROMָ��
	ds18b20_init();
	ds18b20_writeBit(0xCC);         	//����ROMָ��
	ds18b20_writeBit(0x48);         	//���Ƶ�EEPROM
}

/////////��ȡ�¶Ⱥ��ͺ�///////////////
//input��temp���¶�
//		 type���ͺ�
//return��NON
void getTempType(u16 *temp,u8 *type){
	u8 tempt_L,tempt_H;
	ds18b20_init();
	ds18b20_writeBit(0xCC);         	//����ROMָ��
	ds18b20_writeBit(0x44);         	//�¶�ת��ָ��
	delay_ms(1000);
	ds18b20_init();
	ds18b20_writeBit(0xcc);         	//����ROMָ��
	ds18b20_writeBit(0xbe);				//���ݴ���ָ��
	tempt_L = ds18b20_readBit();
	tempt_H = ds18b20_readBit();
	*type = ds18b20_readBit();
	*temp = (tempt_H<<8) + tempt_L;	
	ds18b20_init();
}

////////////��ȡ��������Ϣ////////////////////	
u8 tempFrq = 0;
void get_sensorInf(SENSOR *sensor){
	u8 tempt_L,tempt_H;	 
	ds18b20_init();
	ds18b20_writeBit(0x33);         	//��rom ID
	tempt_L =  ds18b20_readBit();
	tempt_H =  ds18b20_readBit();
	tempt_L =  ds18b20_readBit();
	sensor->id = (tempt_H<<8) + tempt_L;
	
	ds18b20_init();
	ds18b20_writeBit(0xCC);         	//����ROMָ��
	ds18b20_writeBit(0x44);         	//�¶�ת��ָ��
	delay_ms(1000);
	ds18b20_init();
	ds18b20_writeBit(0xcc);         	//����ROMָ��
	ds18b20_writeBit(0xbe);				//���ݴ���ָ��
	tempt_L = ds18b20_readBit();
	tempt_H = ds18b20_readBit();
	sensor->model = ds18b20_readBit();
	sensor->tempt = (tempt_H<<8) + tempt_L;	
	ds18b20_init();
	
	tempFrq = (u8)(sensor->frequency1/1000)+2;
	Timer1_Init();
	delay_ms(1800);
}




