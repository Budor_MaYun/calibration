#include "sensor.h"

void led_shile(void){
	u8 i;
	for(i = 0;i < 3;i++){
		LED1_ON();
		delay_ms(300);
		LED1_OFF();
		delay_ms(200);
	}
}

void sendTempData(SENSOR *sensor,u8 irc){
	u8 tempArr[6];
	LED1_ON();
	tempArr[0] = 0xFB; 
	tempArr[1] = 0xBF;
	tempArr[2] = (sensor->tempt>>8)&0xff;
	tempArr[3] = (sensor->tempt>>0)&0xff;
	tempArr[4] = sensor->model;
	tempArr[5] = irc;
	uartSendNData(tempArr,6);
	LED1_OFF();
}

void sendSensorData(SENSOR *sensor){
	u8 tempArr[9];
	LED1_ON();
	tempArr[0] = 0xBE; 
	tempArr[1] = 0xEB;
	tempArr[2] = (sensor->id>>8)&0xff;
	tempArr[3] = (sensor->id>>0)&0xff;
	tempArr[4] = sensor->model;
	tempArr[5] = (sensor->tempt>>8)&0xff;
	tempArr[6] = (sensor->tempt>>0)&0xff;
	tempArr[7] = (sensor->frequency1>>8)&0xff;
	tempArr[8] = (sensor->frequency1>>0)&0xff;
	//tempArr[9] = CRC_check(tempArr,9);
	uartSendNData(tempArr,9);
	LED1_OFF();
}

u8 CRC_check(u8 arr[],u8 lenth){
	u8 sun = 0,i;
	for(i = 0;i < lenth;i++){
		sun ^= arr[i];
	}
	return sun;
}
